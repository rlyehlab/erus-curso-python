# Curso de introducción a Python

Presentación para el curso introductorio sobre Python dictado en [R'lyeh Hacklab](https://rlab.be), CABA, Argentina.

## Licencia

![CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/88x31.png)  
Todo el contenido se encuentra licenciado bajo *[Atribución-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es)* salvo que se explicite lo contrario (algunos contenidos tienen licencia aparte).
